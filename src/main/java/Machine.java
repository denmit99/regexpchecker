import java.util.ArrayList;

public class Machine {

    private int startState; //Индекс начального состояния автомата
    private int finalState; //Индекс конечного состояния автомата
    private ArrayList<Expression> machineTransitions; //массив всех переходов с указанием, из какого узла он исходит и в какой

    public Machine(Expression exp) { //Создает автомат по заданному выражению
        Numerator numerator = new Numerator();
        ArrayList<Expression> nodes = new ArrayList<Expression>();
        startState = numerator.getNewNumber();
        finalState = numerator.getNewNumber();
        exp.setFrom(startState);
        exp.setTo(finalState);
        nodes.add(exp);
        machineTransitions = exp.extract(numerator);
    }

    public boolean checkChain(String chain) {

        /*
        * Проверка на сооответствие цепочки символов автомату
         */

        char[] symbols = chain.toCharArray();
        int currentState = startState;
        for (int i = 0; i < chain.length(); i++) {
            int nextState;
            while ((nextState = getNextState(symbols[i], currentState)) == -1) {
                nextState = getNextState('e', currentState);
                if (nextState == -1) {
                    return false;
                } else currentState = nextState;
            }
            currentState = nextState;
        }

        while (currentState != finalState) {
            int nextState = getNextState('e', currentState);
            if (nextState == -1) {
                return false;
            } else currentState = nextState;
        }
        return true;
    }

    public int getNextState(char s, int index) {

        /*
         * Возвращает состояние, в которое переходит из текущего состояния index по метке s
         * Если такого нет, возвращает -1
        */

        for (Expression e : machineTransitions) {
            if ((((Symbol) e).getSymbol() == s) && (e.getFrom() == index)) {
                return e.getTo();
            }
        }
        return -1;
    }

}
