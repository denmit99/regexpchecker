public class Numerator {

    /*
    * Объект класса Numerator выдаюет уникальные номера при каждом обращении к нему методом getNewNumber()
    * */

    private int lastNumber = 0;

    public int getNewNumber() {
        int num = lastNumber;
        ++lastNumber;
        return num;
    }
}
