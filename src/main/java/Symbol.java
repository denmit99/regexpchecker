import java.util.ArrayList;

public class Symbol implements Expression {

    private char symbol;
    private int from;
    private int to;


    public Symbol(char symbol) {
        this.symbol = symbol;
    }


    public Symbol(char symbol, int from, int to) {
        this.symbol = symbol;
        this.from = from;
        this.to = to;
    }

    public ArrayList<Expression> extract(Numerator numerator) {
        ArrayList<Expression> nodes = new ArrayList<Expression>();
        nodes.add(new Symbol(symbol, from, to));
        return nodes;
    }

    public char getSymbol() {
        return symbol;
    }

    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }
}
