import java.util.ArrayList;

public class Iteration implements Expression {

    private Expression expression;
    private int from;
    private int to;


    public Iteration(Expression expression) {
        this.expression = expression;
    }


    public Iteration(Expression expression, int from, int to){
        this.expression = expression;
        this.from = from;
        this.to = to;
    }

    public ArrayList<Expression> extract(Numerator numerator) {
        ArrayList<Expression> nodes = new ArrayList<Expression>();
        int newIndex = numerator.getNewNumber();
        expression.setFrom(newIndex);
        expression.setTo(newIndex);
        nodes.add(new Symbol('e', from, newIndex));
        nodes.add(new Symbol('e', newIndex, to));
        nodes.addAll(expression.extract(numerator));
        return nodes;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }
}
