import java.util.ArrayList;

public class Combination implements Expression {

    private Expression leftExpression;
    private Expression rightExpression;
    private int from;
    private int to;


    public Combination(Expression leftExpression, Expression rightExpression) {
        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }


    public Combination(Expression leftExpression, Expression rightExpression, int from, int to) {
        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
        this.from = from;
        this.to = to;
    }

    public ArrayList<Expression> extract(Numerator numerator) {
        ArrayList<Expression> nodes = new ArrayList<Expression>();
        leftExpression.setFrom(from);
        leftExpression.setTo(to);
        rightExpression.setFrom(from);
        rightExpression.setTo(to);
        nodes.addAll(leftExpression.extract(numerator));
        nodes.addAll(rightExpression.extract(numerator));
        return nodes;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }
}
