import java.util.Stack;

/*
 * Программа строит автомат по регулярному выражению
 * и проверяет цепочку на соответствие автомату
 *
 * ******** ВХОДНЫЕ ПАРАМЕТРЫ ********
 * REGEXP - Регулярное выражение, по которому строится автомат
 * CHAIN - Цепочка для проверки
 *
 * ******** СИНТАКСИС РЕГУЛЯРНОГО ВЫРАЖЕНИЯ ********
 *  (a)* - ИТЕРАЦИЯ (Iteration)
 *  (a+b) - ОБЪЕДИНЕНИЕ (Combination)
 *  (a&b) - КОНКАТЕНАЦИЯ (Concatenation)
 *
 *  Для корректной работы рекомендуется использовать скобки
 * */

public class Main {

    public static final String REGEXP = "((a+b)&(c)*)*";
    public static final String CHAIN = "acccccbccacb";

    public static void main(String[] args) {

        String converted = convertToPolishNotation(REGEXP);
        Expression exp = polishNotationToExpression(converted);
        Machine machine = new Machine(exp);
        if (machine.checkChain(CHAIN)) {
            System.out.println("SUCCESS! Machine recognizes this chain!");
        } else {
            System.out.println("FAIL! Machine  doesn't recognizes this chain!");
        }

    }

    public static int getOperatorPriority(char op) {

        /*
         * Возвращает приоритет оператора.
         * Чем больше значение, тем выше приоритет
         * */

        if (op == '+')
            return 1;
        else if (op == '&')
            return 2;
        else if (op == '*')
            return 3;
        else return 0;
    }

    public static String convertToPolishNotation(String exp) {

        /*
         * Переводит входное регулярное выражение в обратную польскую запись для удобства построения автомата
         * */

        String polskline = "";
        char[] regexp = exp.toCharArray();
        Stack<Character> stack = new Stack<Character>();
        for (int i = 0; i < regexp.length; i++) {
            if (regexp[i] == ' ') {
                continue;
            } else if (regexp[i] == '(') {
                stack.push('(');
            } else if (Character.isLetterOrDigit(regexp[i]) || (regexp[i] == '*')) {
                polskline += String.valueOf(regexp[i]);
            } else if (regexp[i] == ')') {
                while (stack.peek().charValue() != '(') {
                    polskline += String.valueOf(stack.pop());
                }
                if (stack.peek().charValue() == '(') {
                    stack.pop();
                }
            } else if ((regexp[i] == '+') || (regexp[i] == '&')) {
                if (i == regexp.length - 1) {
                    System.out.println("Error");
                    break;
                }
                while (getOperatorPriority(regexp[i]) <= getOperatorPriority(stack.peek().charValue())) {
                    polskline += String.valueOf(stack.pop());
                }
                stack.push(regexp[i]);
            } else {
                System.out.println("Error");
                break;
            }
        }
        return polskline;
    }

    public static Expression polishNotationToExpression(String s) {

        /*
         * Строит систему вложенных выражений по входной строке
         * */

        char[] line = s.toCharArray();
        Stack<Expression> stack = new Stack<Expression>();
        Expression expression;
        for (int i = 0; i < line.length; i++) {
            if (Character.isLetterOrDigit(line[i])) {
                stack.push(new Symbol(line[i]));
            } else if (line[i] == '*') {
                stack.push(new Iteration(stack.pop()));
            } else if (line[i] == '+') {

                Expression right = stack.pop();
                Expression left = stack.pop();
                stack.push(new Combination(left, right));
            } else if (line[i] == '&') {

                Expression right = stack.pop();
                Expression left = stack.pop();
                stack.push(new Concatenation(left, right));
            }
        }
        expression = stack.pop();
        return expression;
    }
}


