import java.util.ArrayList;

public interface Expression {

    /*
    * Интерфейс выражения
    * */

    ArrayList<Expression> extract(Numerator numerator);

    void setFrom(int index);
    void setTo(int index);
    int getFrom();
    int getTo();
}
